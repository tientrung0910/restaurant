import React from 'react'
import './Input.css'


const Input = () => {
  return (
    <div className='Input-search'>
      <div className="search-icon">
        <img className="search-icon-item" alt="" src="/searchmd1.svg" />
        <input type="text" className="search-input"placeholder='맛집 이름을 검색해보세요'/>
      </div>
    </div>

);
}

export default Input