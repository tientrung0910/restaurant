
// @/src/pages/api/trpc/[trpc].ts
import * as trpcNext from "@trpc/server/adapters/next";

import { AppRouter } from "@/server/routers/_app";
import { createContext } from "@/server/context";

export default trpcNext.createNextApiHandler({
  router: AppRouter,
  createContext,
});