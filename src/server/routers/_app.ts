
import {  procedure, router } from "@/server/trpc";
import prisma from "../../lib/prisma"
import { z } from "zod";
import { TRPCError } from '@trpc/server';
import { getHTTPStatusCodeFromError } from '@trpc/server/http';

export const AppRouter = router({

  getRestaurants: procedure.query(async () => {
    const restaurants = await prisma.restaurant.findMany({});
    return restaurants;
  }),

  addFavorite: procedure
  .input(
    z.object({
      foodId: z.string(),
    }),
  )
  .mutation(async (opts) => {
    const restaurant = await prisma.restaurant.updateMany({
        where: {
          id: String(opts.input.foodId),
        },
        data: {
          isFavorite: true
        },
      });
      return restaurant;
  }),
});

