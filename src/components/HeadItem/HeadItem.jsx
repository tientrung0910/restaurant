import PropTypes from "prop-types";
import { siteConfig } from "@/configs/site";
import "./HeadItem.css";

const HeadItem = ({
  className = "",
 
}) => {


  return (
    <div>
    
    </div>
  );
};

HeadItem.propTypes = {
  className: PropTypes.string,

  /** Style props */
  headItemPosition: PropTypes.any,
  headItemTop: PropTypes.any,
  headItemLeft: PropTypes.any,
};

export default HeadItem;
