import HeadItem from '../components/HeadItem/HeadItem'
import Input from '../components/Input/Input'
import TypeButtonGraySizesmFul from '../components/TypeButtonGraySizesmFul/TypeButtonGraySizesmFul'
import Footer from '../components/Footer/Footer'
import '../styles/global.css'

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>
        <div>
      <div className="header">
      <HeadItem/>
      <Input/>
      <TypeButtonGraySizesmFul/>
      </div>
        <div className="container">{children}</div>
        <Footer />
        </div>
      </body>
    </html>
  )
}
