import "./Footer.css";
import { siteConfig } from "@/configs/site";

const Footer = ({ className = "" }) => {
  return (
    <div className={`tabbar ${className}`}>
      <div className="tabs">
        <div className="tabbar-item">
          <img
            className="sf-symbol-scribblevariable"
            alt=""
            src={`${siteConfig.url}sf-symbol--scribblevariable.svg`}
          />
          <img className="home-line-icon" alt="" src={`${siteConfig.url}homeline.svg`} />
          <div className="sf-symbol">􀤑</div>
          <div className="label1">홈</div>
        </div>
        <div className="tabbar-item">
          <img
            className="sf-symbol-scribblevariable"
            alt=""
            src={`${siteConfig.url}sf-symbol--scribblevariable.svg`}
          />
          <img className="home-line-icon" alt="" src={`${siteConfig.url}searchmd.svg`} />
          <div className="sf-symbol">􀤑</div>
          <div className="label2">검색</div>
        </div>
        <div className="tabbar-item">
          <img
            className="sf-symbol-scribblevariable"
            alt=""
            src={`${siteConfig.url}sf-symbol--scribblevariable.svg`}
          />
          <img
            className="home-line-icon"
            alt=""
            src={`${siteConfig.url}messagetextsquare01.svg`}
          />
          <div className="sf-symbol">􀤑</div>
          <div className="label1">피드</div>
        </div>
        <div className="tabbar-item">
          <img
            className="sf-symbol-scribblevariable"
            alt=""
            src={`${siteConfig.url}sf-symbol--scribblevariable.svg`}
          />
          <img className="home-line-icon" alt="" src={`${siteConfig.url}calendar.svg`} />
          <div className="sf-symbol">􀤑</div>
          <div className="label1">내 예약</div>
        </div>
        <div className="tabbar-item">
          <img
            className="sf-symbol-scribblevariable"
            alt=""
            src={`${siteConfig.url}sf-symbol--scribblevariable.svg`}
          />
          <img className="home-line-icon" alt="" src={`${siteConfig.url}menu01.svg`} />
          <div className="sf-symbol">􀤑</div>
          <div className="label1">메뉴</div>
        </div>
      </div>
     
    </div>
  );
};



export default Footer;
