'use client'
import { useRouter } from "next/navigation"
import { useMemo } from "react";
import "./SizesmHierarchySecondaryG.css";
import { trpc } from '@/utils/trpc';
// import React, { useEffect, useState } from "react";

const SizesmHierarchySecondaryG = ({
  className = "",
  foodId,
  placeholder,
  sizesmHierarchySecondaryGBoxShadow,
  sizesmHierarchySecondaryGBorderRadius,
  sizesmHierarchySecondaryGBackgroundColor,
  sizesmHierarchySecondaryGBorder,
  sizesmHierarchySecondaryGPosition,
  sizesmHierarchySecondaryGRight,
  sizesmHierarchySecondaryGBottom,
  sizesmHierarchySecondaryGBackdropFilter,
}) => {
  const sizesmHierarchySecondaryGStyle = useMemo(() => {
    return {
      boxShadow: sizesmHierarchySecondaryGBoxShadow,
      borderRadius: sizesmHierarchySecondaryGBorderRadius,
      backgroundColor: sizesmHierarchySecondaryGBackgroundColor,
      border: sizesmHierarchySecondaryGBorder,
      position: sizesmHierarchySecondaryGPosition,
      right: sizesmHierarchySecondaryGRight,
      bottom: sizesmHierarchySecondaryGBottom,
      backdropFilter: sizesmHierarchySecondaryGBackdropFilter,
    };
  }, [
    sizesmHierarchySecondaryGBoxShadow,
    sizesmHierarchySecondaryGBorderRadius,
    sizesmHierarchySecondaryGBackgroundColor,
    sizesmHierarchySecondaryGBorder,
    sizesmHierarchySecondaryGPosition,
    sizesmHierarchySecondaryGRight,
    sizesmHierarchySecondaryGBottom,
    sizesmHierarchySecondaryGBackdropFilter,
  ]);
  const router = useRouter()
  async function addFavorite(foodId) {
  await fetch(`http://localhost:3000/api/trpc/addFavorite`, {
    headers: { 'Content-Type': 'application/json' },
    method: 'POST',
    body: JSON.stringify({foodId: foodId })
  })
  router.push('/')
  alert("your favorite is added!");
  };

  return (
    <div
      className={`sizesm-hierarchysecondary-g ${className}`}
      style={sizesmHierarchySecondaryGStyle}
    >
      <img className="placeholder-icon" onClick={() => addFavorite(foodId)} alt="" src={placeholder} />
    </div>
  );
};


export default SizesmHierarchySecondaryG;
