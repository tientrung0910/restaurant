# Restaurant test with Next.js (TRPC + PRISMA + POSTGRESQL)
## Getting started

### 1. Download example and install dependencies

```
npm install
```
### 3. Start the app

```
npm run dev
```

The app is now running, navigate to [`http://localhost:3000/`](http://localhost:3000/) in your browser to explore its UI.


**Home** (located in [`/`](./pages/index.tsx))


## Using the TRPC API

You can also access the REST API of the API server directly. It is running on the same host machine and port and can be accessed via the `/api/trpc` route (in this case that is `http://localhost:3000/api/trpc`)

### `GET`

- `/getRestaurants`: Fetch all restaurants

### `POST`

- `/addFavorite`: Add favorite
  - Body:
    - `foodId: String`
