import SizesmHierarchySecondaryG from "../SizesmHierarchySecondaryG/SizesmHierarchySecondaryG.jsx";
import { siteConfig } from "@/configs/site";
import "./FrameComponent.css";
function FrameComponent ({ food }) {
  var heart = `/heart_${food.isFavorite}.svg`;
  return (
    <div className={`image-group`}>
      <div className="image1" style={{ 'background-color': 'grey', 'background-image': `url(${food.images[0]})` }}>
        <SizesmHierarchySecondaryG
        foodId={food.id}
          placeholder={heart}
          sizesmHierarchySecondaryGBoxShadow="unset"
          sizesmHierarchySecondaryGBorderRadius="40px"
          sizesmHierarchySecondaryGBorder="unset"
          sizesmHierarchySecondaryGPosition="absolute"
          sizesmHierarchySecondaryGRight="8px"
          sizesmHierarchySecondaryGBottom="156px"
          sizesmHierarchySecondaryGBackdropFilter="blur(8px)"
        />
      </div>
      <div className="frame-container">
        <div className="group">
          <div className="div5">{`${food.name}`}</div>
          <div className="frame-div">
            <div className="star-icon-container">
              <img className="star-icon1" alt="" src={`${siteConfig.url}star-icon.svg`} />
            </div>
            <div className="div6">{`${food.rating}`}({`${food.rating_count}`})</div>
          </div>
        </div>
        <div className="div7">{`${food.desc}`}</div>
      </div>
    </div>
  );
};


export default FrameComponent;
