import FrameComponent from '../FrameComponent/FrameComponent';

import prisma from "../../lib/prisma"

export default async function Home() {
  const restaurants = await prisma.restaurant.findMany({});
  return (
    <div>
        {restaurants.map((item) => (
          <div key={item.id}>
        < FrameComponent food={item}  />
        </div>
    ))}
    </div>

  );
}
