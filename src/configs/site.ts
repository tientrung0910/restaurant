export type SiteConfig = typeof siteConfig;

export const siteConfig = {
  name: "Demo restaurant",
  description:
    "Demo restaurant.",
  url: process.env.SITE_URL
};
