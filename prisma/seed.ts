import { PrismaClient } from '@prisma/client'
import { restaurants } from './restaurants'
const prisma = new PrismaClient()
export async function seedRun(){
  await prisma.restaurant.createMany({
    data: restaurants
  });
  console.log('Added product data');
} 

seedRun();